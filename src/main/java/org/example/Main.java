package org.example;

import org.example.dao.AddressDao;
import org.example.dao.PersonDao;
import org.example.entities.Address;
import org.example.entities.AddressId;
import org.example.entities.BuildingType;
import org.example.entities.Person;

public class Main {

    private static AddressDao addressDao = new AddressDao();
    private static PersonDao personDao = new PersonDao();

    public static void main(String[] args) {

        AddressId addressId = new AddressId("Primaverii", 14, "Cluj-Napoca");
        Address address = new Address(addressId, 150, BuildingType.PRIVATE_HOUSE);
        addressDao.createAddress(address);

        AddressId addressId2 = new AddressId("Dorobanți", 54, "București");
        Address address2 = new Address(addressId2, 5000, BuildingType.OFFICE);
        addressDao.createAddress(address2);

        AddressId addressId3 = new AddressId("Eroilor", 12, "Timișoara");
        Address address3 = new Address(addressId3, 1200, BuildingType.RESIDENTIAL);
        addressDao.createAddress(address3);


        Person ion = new Person("193040567842345", "Ion", 1993, address);
        Person vlad = new Person("177040567842345", "Vlad", 1977, address2);
        Person mihai = new Person("186040567842345", "Mihai", 1986, address3);
        Person dorin = new Person("168040567842345", "Dorin", 1968, address2);
        Person maria = new Person("289121298983556", "Maria", 1989, address3);
        Person ioana = new Person("299121298983556", "Ioana", 1999, address2);
        Person simona = new Person("278121298983556", "Simona", 1978, address);

        personDao.createPerson(ion);
        personDao.createPerson(vlad);
        personDao.createPerson(mihai);
        personDao.createPerson(dorin);
        personDao.createPerson(maria);
        personDao.createPerson(ioana);
        personDao.createPerson(simona);


        personDao.readAllPerson().forEach(p -> prettyDisplayOfPerson(p));
        System.out.println("*********************************************");
        maria.setName("Maricica");
        personDao.updatePerson(maria);
        personDao.readAllPerson().forEach(p -> prettyDisplayOfPerson(p));

        System.out.println("All males");
        personDao.getAllMales().forEach(System.out::println);


        System.out.println("YOUNG");
        personDao.youngerThan(1990).forEach(System.out::println);

        System.out.println("Din Cluj-Napoca");
        personDao.fromCity("Cluj-Napoca").forEach(System.out::println);

    }

    static void prettyDisplayOfPerson(Person p) {
        System.out.println(
                p.getName() +
                        " născut în " + p.getYearOfBirth() +
                        " locuiește în " + p.getAddress().getId().getCity());
    }

    static void prettyPrint(Integer nr, String traducere) {
        System.out.println("+++ Numărul " + nr + " se pronunță " + traducere + " ++++");

    }


}